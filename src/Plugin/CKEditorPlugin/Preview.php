<?php

namespace Drupal\ckeditor_preview\Plugin\CKEditorPlugin;

use Drupal\editor\Entity\Editor;
use Drupal\ckeditor\CKEditorPluginBase;

/**
 * Defines the "preview" plugin.
 *
 * @CKEditorPlugin(
 *   id = "preview",
 *   label = @Translation("CKEditor Preview"),
 *   module = "ckeditor_preview"
 * )
 */
class Preview extends CKEditorPluginBase {

  /**
   * Implements \Drupal\ckeditor\Plugin\CKEditorPluginInterface::getFile().
   */
  public function getFile() {
    return 'libraries/' . $this->getPluginId() . '/plugin.js';
  }

  /**
   * {@inheritdoc}
   */
  public function getDependencies(Editor $editor) {
    return [];
  }

  /**
   * {@inheritdoc}
   */
  public function getLibraries(Editor $editor) {
    return [];
  }

  /**
   * Implements CKEditorPluginButtonsInterface::getButtons().
   */
  public function getButtons() {
    return [
      'Preview' => [
        'label' => $this->t('Preview'),
        'image' => 'libraries/' . $this->getPluginId() . '/icons/preview.png',
      ],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getConfig(Editor $editor) {
    return [];
  }

}
