CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Installation
 * Configuration
 * Maintainers

INTRODUCTION
------------

CKEditor Preview module adds toolbar button which shows a preview of the
document as it will be displayed to end users or printed.

 * For a full description of the module, visit the project page:
   https://www.drupal.org/project/ckeditor_preview

 * To submit bug reports and feature suggestions, or to track changes:
   https://www.drupal.org/project/issues/ckeditor_preview

REQUIREMENTS
------------

 * [CKEditor Preview library](http://ckeditor.com/addon/preview)

INSTALLATION
------------

 1. Download and install CKEditor Preview module.

    Install as you would normally install a contributed Drupal module.
  	See: https://www.drupal.org/node/895232 for further information.

 2. Install the [CKEditor Preview library](http://ckeditor.com/addon/preview)
    
    For Composer-managed Drupal installations, the recommended method is to use
    the [Composer Merge Plugin](https://github.com/wikimedia/composer-merge-plugin)
    and this module's `composer.libraries.json` file. From a Composer project 
    root:
    
    1. Execute `composer require wikimedia/composer-merge-plugin`.
    2. Add the following to the `extra` section of the root `composer.json` 
       file:
    
        ```
        "merge-plugin": {
            "include": [
                "{DOCROOT}/modules/contrib/ckeditor_preview/composer.libraries.json"
            ]
        },
        ```
    
        Note: Remember to replace `{DOCROOT}` with the appropriate root folder 
        for the Drupal installation -- this is likely `web` or `docroot`.
    3. Execute `composer install` (or, in some cases, `composer update --lock`).
    
    That's it! Composer should install the CKEditor CodeMirror plugin in the 
    appropriate place (`/libraries/preview`).

CONFIGURATION
-------------

 1. Go to **Administration » Configuration » Content authoring » Text formats
    and editors** (admin/config/content/formats).
 2. Click *Configure* for any text format using CKEditor as the text editor.
 3. Add the Preview button to the toolbar. 
 4. Scroll down and click **Save configuration**.
 5. Go to node create/edit page, choose the text format with Preview plugin.
    Press the "Preview" button.

MAINTAINERS
-----------

Current maintainers:
 * Rahul Rasgon (https://www.drupal.org/u/rahulrasgon)
 
 This project has been sponsored by:
 * QED42  
   **QED42** is a web development agency focused on helping organizations and
   individuals reach their potential, most of our work is in the space of
   publishing, e-commerce, social and enterprise.
